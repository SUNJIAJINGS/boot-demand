package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.ExaminationApply;
import java.util.List;

public interface ExaminationApplyMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationApply row);

    ExaminationApply selectByPrimaryKey(Integer id);

    List<ExaminationApply> selectAll();

    int updateByPrimaryKey(ExaminationApply row);
}