package com.bbyb.operating.examination.service;

import com.bbyb.operating.examination.model.po.User;

import java.util.List;

public interface UserService {
    String addUser(User user);

    List<User> getAllUser();



}
