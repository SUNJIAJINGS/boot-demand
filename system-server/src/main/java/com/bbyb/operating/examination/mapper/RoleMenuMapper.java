package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.RoleMenu;
import java.util.List;

public interface RoleMenuMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RoleMenu row);

    RoleMenu selectByPrimaryKey(Integer id);

    List<RoleMenu> selectAll();

    int updateByPrimaryKey(RoleMenu row);
}