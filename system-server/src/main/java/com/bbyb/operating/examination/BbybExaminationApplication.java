package com.bbyb.operating.examination;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.bbyb.operating.examination.mapper")
public class BbybExaminationApplication {

    public static void main(String[] args) {
        SpringApplication.run(BbybExaminationApplication.class, args);
        System.out.printf("启动成功================================================================================================================================================");
    }

}
