package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.ExaminationSubject;
import java.util.List;

public interface ExaminationSubjectMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(ExaminationSubject row);

    ExaminationSubject selectByPrimaryKey(Integer id);

    List<ExaminationSubject> selectAll();

    int updateByPrimaryKey(ExaminationSubject row);
}