package com.bbyb.operating.examination.controller;

import com.bbyb.operating.examination.model.po.Role;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.model.vo.CommonResult;
import com.bbyb.operating.examination.service.UserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 账号
 * className: UserController
 * datetime: 2023/2/10 14:28
 * author: lx
 */
@RestController()
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping(value = "add")
    public CommonResult<Boolean> addUser(@RequestBody User user){
        if(user == null){
            return new CommonResult<>(601, "用户信息不能为空");
        }

        if(StringUtils.isBlank(user.getUserCode())){
            return new CommonResult<>(601, "用户编码不能为空");
        }

        if(StringUtils.isBlank(user.getUserName())){
            return new CommonResult<>(601, "用户名不能为空");
        }

        if(StringUtils.isBlank(user.getPhone())){
            return new CommonResult<>(601, "用户手机号不能为空");
        }

        if(StringUtils.isBlank(user.getPassword())){
            return new CommonResult<>(601, "用户密码不能为空");
        }

        String errorMsg = userService.addUser(user);
        if(StringUtils.isNotEmpty(errorMsg)){
            return new CommonResult<>(603, errorMsg);
        }
        return new CommonResult<>(true);
    }


    @PostMapping("/allUser")
    public List<User> getAllUser(){
        return userService.getAllUser();
    }












}



