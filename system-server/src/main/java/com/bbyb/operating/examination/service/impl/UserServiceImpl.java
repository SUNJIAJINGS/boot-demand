package com.bbyb.operating.examination.service.impl;

import com.bbyb.operating.examination.mapper.UserMapper;
import com.bbyb.operating.examination.model.po.User;
import com.bbyb.operating.examination.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 账号服务
 * className: UserServiceImpl
 * datetime: 2023/2/10 14:29
 * author: lx
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public String addUser(User user) {
        if(userMapper.insert(user) == 1){
            return null;
        }
        return "保存用户信息失败";
    }

    @Override
    public List<User> getAllUser() {
        return userMapper.selectAll();
    }





}
