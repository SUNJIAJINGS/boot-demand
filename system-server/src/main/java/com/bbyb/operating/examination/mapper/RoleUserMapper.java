package com.bbyb.operating.examination.mapper;

import com.bbyb.operating.examination.model.po.RoleUser;
import java.util.List;

public interface RoleUserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(RoleUser row);

    RoleUser selectByPrimaryKey(Integer id);

    List<RoleUser> selectAll();

    int updateByPrimaryKey(RoleUser row);
}